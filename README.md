# springboot-app-1

This is using the JGitflow Maven plugin of Atlassian for an easier
workflow on snapshots and releases

- `mvn jgitflow:feature-start`, creates a new feature branch
- `mvn jgitflow:feature-finish`, will delete the feature branch and back to `development`
- `mvn jgitflow:release-start`, creates a release branch
- `mvn jgitflow:release-finish`, makes a release and tags it then increments snapshots

This is using `s3-storage-wagon` that will upload snapshots and releases to AWS S3 Bucket which
is cheaper compared to hosted Maven repositories.

- ensure to set region of Bucket like `export AWS_REGION=ap-southeast-1`
- execute `mvn clean deploy` to upload snapshots to S3
